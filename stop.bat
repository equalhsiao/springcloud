::"C:\Program Files\Git\bin\bash.exe" --login -c "docker-compose -f docker-gitlab.yml down"
"C:\Program Files\Git\bin\bash.exe" --login -c "docker-compose -f docker-rabbitmq.yml down"
"C:\Program Files\Git\bin\bash.exe" --login -c "docker-compose -f docker-zipkin.yml down"
"C:\Program Files\Git\bin\bash.exe" --login -c "docker stop eurekaserver && docker rm eurekaserver"
"C:\Program Files\Git\bin\bash.exe" --login -c "docker stop configserver && docker rm configserver"
"C:\Program Files\Git\bin\bash.exe" --login -c "docker stop feignserver && docker rm feignserver"
"C:\Program Files\Git\bin\bash.exe" --login -c "docker service rm mailserver"
pause