*   use gmail app password
    1. 到google帳戶中開啟低安全性應用程式存取權
    2. 啟用兩段式驗證
    3. 設定app password
*   docker command:
    1. cd {mailServer path}
    2. docker build -t mailserver . 
    3. docker run -p 8080:8080 -e SPRING_MAIL_USERNAME=e{usermail}-e SPRING_MAIL_PASSWORD={usermail password} mailserver

*   eclipse -> project -> com.tap.mailserver.MailServerApplication -> Run Configuractions -> Arguments
    1. program arguments add:
       -Dspring.mail.password=gztlqfeqygxxwmzb -Dspring.mail.username=equal.hsiao@tapsystem.co

*   swagger ui
    http://192.168.99.100:8080/swagger-ui.html
*   手動更新設定
    http://192.168.99.100:8080/actuator/refresh
*   docker compose
```
version: "3.7"
services:
    mailserver:
        hostname: mailserver
        container_name: mailserver
        ports:
            - '8080:8080'
        environment:
            - SPRING_MAIL_PASSWORD=gztlqfeqygxxwmzb
            - SPRING_MAIL_USERNAME=equal.hsiao@tapsystem.co
            - JVM_OPTS=-Xmx256m -Xms768m -XX:MaxPermSize=512m
        volumes:
            - '/c/Users/xyz90/Desktop:/app/tmp'
        image: mailserver
        tty: true
```