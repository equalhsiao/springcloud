package com.tap.mailserver.aop.impl;

import com.tap.mailserver.dto.MailFormatEnum;
import com.tap.mailserver.aop.MailFormatEnumSet;
import java.util.Arrays;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class MailFormatEnumValid implements ConstraintValidator<MailFormatEnumSet, MailFormatEnum> {

	private MailFormatEnum[] mailFormatEnum;

	@Override
	public void initialize(MailFormatEnumSet constraint) {
		this.mailFormatEnum = constraint.anyOf();
	}

	@Override
	public boolean isValid(MailFormatEnum value, ConstraintValidatorContext context) {
		return value == null || Arrays.asList(mailFormatEnum).contains(value);
	}

}
