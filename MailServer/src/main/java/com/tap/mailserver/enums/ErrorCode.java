package com.tap.mailserver.enums;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

public class ErrorCode {
	public static final int success = 0;
	// TAP Code
	public static final int NeoServerError = 901; // UC service return error
	public static final int ParamNotFound = 902; // 參數未正確填寫

	public static Map<String, Integer> instance;

	static {
		instance = new HashMap<>();
		Field[] fields = ErrorCode.class.getDeclaredFields();
		for (Field field : fields) {
			if (field.getType() == int.class) {
				try {
					instance.put(field.getName(), field.getInt(null));
				} catch (IllegalArgumentException | IllegalAccessException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
