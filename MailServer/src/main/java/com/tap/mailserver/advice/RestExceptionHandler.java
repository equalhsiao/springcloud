package com.tap.mailserver.advice;

import com.tap.mailserver.dto.ResObj;
import com.tap.mailserver.enums.ErrorCode;
import com.tap.mailserver.exception.ParamNotFoundException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 
 * @author equalhsiao
 *
 */
@RestControllerAdvice
public class RestExceptionHandler {
	@ExceptionHandler(value = { Exception.class })
	public ResObj neoServerError(Exception e) {
		e.printStackTrace(); // 開啟錯誤提示 比較好找bug
		return ResObj.response(ErrorCode.NeoServerError, e.getMessage());
	}

	@ExceptionHandler(value = { ParamNotFoundException.class })
	public ResObj paramNotFoundError(ParamNotFoundException e) {
		return ResObj.response(ErrorCode.ParamNotFound, e.getMessage());
	}
}
