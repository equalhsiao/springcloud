//package com.tap.MailServer.controller;
//
//import java.security.KeyManagementException;
//import java.security.NoSuchAlgorithmException;
//import java.security.cert.CertificateException;
//import java.security.cert.X509Certificate;
//import java.util.List;
//
//import javax.net.ssl.SSLContext;
//import javax.net.ssl.TrustManager;
//import javax.net.ssl.TrustManagerFactory;
//import javax.net.ssl.X509TrustManager;
//
//import org.apache.http.conn.socket.LayeredConnectionSocketFactory;
//import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
//import org.apache.http.impl.client.CloseableHttpClient;
//import org.apache.http.impl.client.HttpClients;
//import org.springframework.core.ParameterizedTypeReference;
//import org.springframework.http.HttpEntity;
//import org.springframework.http.HttpHeaders;
//import org.springframework.http.HttpMethod;
//import org.springframework.http.MediaType;
//import org.springframework.http.ResponseEntity;
//import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.client.RestTemplate;
//
//import com.tap.MailServer.dto.ResObj;
//import com.tap.MailServer.enums.ErrorCode;
//
//import lombok.extern.slf4j.Slf4j;
//
//@RestController
//public class TestController {
//
//	@PostMapping(path = "/getData", consumes = "application/json", produces = "application/json")
//	public ResObj sendMail() throws Exception {
//		String url = "https://data.ntpc.gov.tw/api/v1/rest/datastore/382000000A-000077-002";
//        HttpComponentsClientHttpRequestFactory requestFactory =
//                new HttpComponentsClientHttpRequestFactory();
//        requestFactory.setHttpClient(createHttpsClient());
//		RestTemplate restTemplate = new RestTemplate(requestFactory);
//		HttpHeaders headers = new HttpHeaders();
//		
//		headers.setContentType(MediaType.APPLICATION_JSON);
//		HttpEntity<String> request = new HttpEntity<>(headers);
//
//		ParameterizedTypeReference<Object> companyProfileRespType = new ParameterizedTypeReference<Object>() {
//		};
//
//		ResponseEntity<Object> response = restTemplate.exchange(url, HttpMethod.GET, request,
//				companyProfileRespType);
//		System.out.println(response);
//		return ResObj.response(ErrorCode.success);
//	}
//	
//    /**
//     * 解決 PKIX path building failed 問題
//     * 
//     * @return HttpClient
//     * @throws NoSuchAlgorithmException
//     * @throws KeyManagementException
//     */
//    private CloseableHttpClient createHttpsClient() throws NoSuchAlgorithmException, KeyManagementException {
//        // Create a trust manager that does not validate certificate chains
//        TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
//            @Override
//            public void checkClientTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
//            }
//
//            @Override
//            public void checkServerTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
//            }
//
//            @Override
//            public X509Certificate[] getAcceptedIssuers() {
//                return null;
//            }
//        } };
//
//        SSLContext ctx = SSLContext.getInstance("TLS");
//        ctx.init(null, trustAllCerts, null);
//
//        LayeredConnectionSocketFactory sslSocketFactory = new SSLConnectionSocketFactory(ctx);
//
//        CloseableHttpClient httpclient = HttpClients.custom().setSSLSocketFactory(sslSocketFactory).build();
//        return httpclient;
//    }
//}
