package com.tap.mailserver.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RefreshScope
public class ConfigController {

	@Value("${spring.servlet.multipart.maxFileSize}")
	private String maxFileSize;

	@Value("${spring.servlet.multipart.maxRequestSize}")
	private String maxRequestSize;

	@GetMapping("maxFileSize")
	public String maxFileSize() {
		return maxFileSize;
	}

	@GetMapping("maxRequestSize")
	public String maxRequestSize() {
		return maxRequestSize;
	}

}
