package com.tap.mailserver.controller;

import com.tap.mailserver.dto.Mail;
import com.tap.mailserver.dto.ResObj;
import com.tap.mailserver.enums.ErrorCode;
import com.tap.mailserver.exception.ParamNotFoundException;
import com.tap.mailserver.service.MailService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.File;
import java.io.InputStream;
import java.util.Base64;
import java.util.Map;
import java.util.Optional;

/**
 * @author equalhsiao
 */
//@CrossOrigin(origins = {"http://localhost:7777", "http://someserver:8080"})
@Slf4j
@RestController
public class MailController {

    @Autowired
    MailService mailService;

    // TODO Cross-Origin Resource Sharing設定
    @PostMapping(path = "/sendMailWithFile", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResObj sendMailWithFile(@Valid @RequestPart("mail") Mail mail, @RequestParam("files") MultipartFile[] files)
            throws Exception {
        Optional.ofNullable(mail).orElseThrow(ParamNotFoundException::new);
        mailService.sendMail(mail, files);
        return ResObj.response(ErrorCode.success);
    }

    // TODO Cross-Origin Resource Sharing設定
    @PostMapping(path = "/sendMail", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResObj sendMail(@Valid @RequestBody Mail mail) throws Exception {
        Optional.ofNullable(mail).orElseThrow(ParamNotFoundException::new);
        mailService.sendMail(mail, null);
        return ResObj.response(ErrorCode.success);
    }

}
