package com.tap.mailserver.service.impl;

import com.tap.mailserver.dto.AttachmentFile;
import com.tap.mailserver.dto.AttachmentUrl;
import com.tap.mailserver.dto.Mail;
import com.tap.mailserver.service.MailService;

import java.io.File;
import java.net.URL;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.activation.URLDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.context.Context;

import lombok.extern.slf4j.Slf4j;

/**
 * @author equalhsiao
 */
@Service
@Slf4j
public class MailServiceImpl implements MailService {

    @Autowired
    private JavaMailSender mailSender;
    /**
     * 用來發送模版郵件
     */
    @Autowired
    private ITemplateEngine templateEngine;

    @Value("classpath:static/images/logo.png")
    private Resource logo;

    @Value("classpath:static/images/test.png")
    private Resource test;

    @Value("classpath:static/images/maintenance.png")
    private Resource maintenance;

    /**
     * 發送email
     */
    @Override
    public void sendMail(Mail mail, MultipartFile[] files) throws Exception {
        Context context = new Context();
        mail.getVariables().put("templateName", mail.getTemplateName());
        context.setVariables(mail.getVariables());
        String emailContent = templateEngine.process("main", context);
        MimeMessagePreparator messagePreparator = mimeMessage -> {
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true, "UTF-8");
            helper.setTo(mail.getTo());
            helper.setSubject(mail.getSubject());
            helper.setText(emailContent, true);

            helper.addInline("logo", new FileSystemResource(test.getFile()), "image/png");

            if ("systemMaintenance".equals(mail.getTemplateName())) {
                helper.addInline("maintenance", new FileSystemResource(maintenance.getFile()), "image/png");
            }

            // 透過HTTP直接傳遞檔案
            if (Optional.ofNullable(files).isPresent()) {
                for (MultipartFile file : files) {
                    ByteArrayResource resource = new ByteArrayResource(IOUtils.toByteArray(file.getInputStream()));
                    helper.addAttachment(file.getOriginalFilename(), resource);
                }
            }
            // TODO 透過路徑夾帶檔案有資安疑慮或許用JWT建立目錄或者寄件後移除檔案
            Set<AttachmentFile> attachmentFileSet = Optional.ofNullable(mail.getAttachmentFile())
                    .orElse(new HashSet<AttachmentFile>());
            for (AttachmentFile attachmentFile : attachmentFileSet) {
                FileSystemResource file = new FileSystemResource(new File(attachmentFile.getSourceFilePath()));
                String attachmentFormat = attachmentFile.getAttachmentFileFormat().getFormat().toLowerCase();
                helper.addAttachment(attachmentFile.getAttachmentFileName() + "." + attachmentFormat, file);
            }
            Set<AttachmentUrl> attachmentUrlSet = Optional.ofNullable(mail.getAttachmentUrl())
                    .orElse(new HashSet<AttachmentUrl>());
            for (AttachmentUrl attachmentUrl : attachmentUrlSet) {
                URLDataSource uds = new URLDataSource(new URL(attachmentUrl.getSourceUrl()));
                String attachmentFormat = attachmentUrl.getAttachmentFileFormat().getFormat().toLowerCase();
                helper.addAttachment(attachmentUrl.getAttachmentFileName() + "." + attachmentFormat, uds);
            }
        };
        mailSender.send(messagePreparator);
        log.info("send mail success,mail content:" + mail);
    }


    private void addInline(String cid, File file, MimeMultipart multipart) throws MessagingException {
        DataHandler dataHandler = new DataHandler(new FileDataSource(file));
        MimeBodyPart messageBodyPart = new MimeBodyPart();
        messageBodyPart.setDataHandler(dataHandler);
        messageBodyPart.setContentID("<" + cid + ">");
        messageBodyPart.setDisposition(MimeBodyPart.INLINE);
        multipart.addBodyPart(messageBodyPart);
    }

}
