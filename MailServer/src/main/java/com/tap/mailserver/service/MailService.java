package com.tap.mailserver.service;

import com.tap.mailserver.dto.Mail;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 * 
 * @author equalhsiao
 *
 */
@Service
public interface MailService {
	/**
	 * 
	 * @param mail  信件內容dto
	 * @param files 附帶檔案
	 * @throws Exception 錯誤處理
	 */
	void sendMail(Mail mail, MultipartFile[] files) throws Exception;
}
