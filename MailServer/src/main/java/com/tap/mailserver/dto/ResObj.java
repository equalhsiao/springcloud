package com.tap.mailserver.dto;

import com.tap.mailserver.enums.ErrorCode;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;


public class ResObj extends Base {

	public static ResObj response(int code) {
		ResObj res = new ResObj();
		res.code = code;
		res.data = new HashMap<String, Object>();
		res.errId = StringUtils.EMPTY;
		res.errMsg = StringUtils.EMPTY;
		if (code != ErrorCode.success) {
			res.errId = UUID.randomUUID().toString();
		}

		res.isSuccess = code == ErrorCode.success;
		return res;
	}

	public static ResObj response(int code, String errorMsg) {
		ResObj res = new ResObj();
		res.code = code;
		res.data = new HashMap<String, Object>();
		res.errId = StringUtils.EMPTY;
		res.errMsg = errorMsg;
		if (code != ErrorCode.success) {
			res.errId = UUID.randomUUID().toString();
		}
		res.isSuccess = code == ErrorCode.success;
		return res;
	}

	public static ResObj response(NeoResObj neoResult) {
		ResObj res = new ResObj();
		res.code = neoResult.getCode();
		res.isSuccess = neoResult.isSuccess();
		res.data = new HashMap<String, Object>();
		res.errId = StringUtils.EMPTY;
		res.errMsg = neoResult.getErrMsg();
		if (!neoResult.isSuccess()) {
			res.errId = UUID.randomUUID().toString();
		}
		return res;
	}

	public static ResObj response(NeoMultipResObj neoResult) {
		ResObj res = new ResObj();
		res.code = neoResult.isSuccess() ? ErrorCode.success : ErrorCode.NeoServerError;
		res.isSuccess = neoResult.isSuccess();
		res.data = new HashMap<String, Object>();
		res.errId = StringUtils.EMPTY;
		res.errMsg = neoResult.getRETURNVALUE();
		if (!neoResult.isSuccess()) {
			res.errId = UUID.randomUUID().toString();
		}
		return res;
	}

	public Object getData(String key) {
		return data.get(key);
	}

	public void putData(String key, Object value) {
		this.data.put(key, value);
	}

	public void putData(Map<String, Object> data) {
		this.data = data;
	}
}
