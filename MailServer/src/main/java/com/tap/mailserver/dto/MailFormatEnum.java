package com.tap.mailserver.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.tap.mailserver.exception.ParamNotFoundException;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum MailFormatEnum {

	PDF("pdf"), DOCS("docs"), TXT("txt"),JSON("json"),JPG("jpg"),PNG("png");

	private String format;

	@JsonCreator
	public static MailFormatEnum formatText(String format) {
		format = format.toLowerCase();
		for (MailFormatEnum mailFormatEnum : MailFormatEnum.values()) {
			if (mailFormatEnum.getFormat().toString().equals(format)) {
				return mailFormatEnum;
			}
		}
		throw new ParamNotFoundException("夾帶檔案格式錯誤");
	}
}
