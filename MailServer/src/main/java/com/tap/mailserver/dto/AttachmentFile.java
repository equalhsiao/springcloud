package com.tap.mailserver.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.tap.mailserver.aop.MailFormatEnumSet;
import com.tap.mailserver.exception.ParamNotFoundException;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Data;
import lombok.ToString;

/**
 * 
 * @author equalhsiao
 *
 */
@Data
@ToString
public class AttachmentFile {
	// 夾帶檔案名稱
	@NotBlank
	@JsonProperty(required = true)
	private String attachmentFileName;

	// 檔案來源路徑
	@NotBlank
	@JsonProperty(required = true)
	private String sourceFilePath;

	@NotNull
	@MailFormatEnumSet(anyOf = {
			MailFormatEnum.DOCS,
			MailFormatEnum.PDF,
			MailFormatEnum.TXT,
			MailFormatEnum.JSON,
			MailFormatEnum.JPG,
			MailFormatEnum.PNG})
	@JsonProperty(required = true)
	private MailFormatEnum attachmentFileFormat;

	// 驗證路徑檔案後是否為合法格式
	public void setSourceFilePath(String sourceFilePath) {
		if (sourceFilePath.indexOf('.') <= 0) {
			throw new ParamNotFoundException("來源檔案格式錯誤");
		}
		int lastIndex = sourceFilePath.lastIndexOf('.');
		String fileFormat = sourceFilePath.substring(lastIndex);
		if (fileFormat.startsWith(".")) {
			MailFormatEnum.formatText(fileFormat.replaceFirst("\\.", ""));
		}
		this.sourceFilePath = sourceFilePath;
	}
}
