package com.tap.mailserver.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.tap.mailserver.aop.MailFormatEnumSet;
import com.tap.mailserver.exception.ParamNotFoundException;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class AttachmentUrl {

  @NotBlank
  @JsonProperty(required = true)
  private String attachmentFileName;

  @NotBlank
  @JsonProperty(required = true)
  private String sourceUrl;

  @MailFormatEnumSet(anyOf = {
      MailFormatEnum.DOCS,
      MailFormatEnum.PDF,
      MailFormatEnum.TXT,
      MailFormatEnum.JSON,
      MailFormatEnum.JPG,
      MailFormatEnum.PNG})
  @NotNull
  @JsonProperty(required = true)
  private MailFormatEnum attachmentFileFormat;

  public void setSourceUrl(String sourceUrl) {
    if (sourceUrl.indexOf('.') <= 0) {
      throw new ParamNotFoundException("來源檔案格式錯誤");
    }
    int lastIndex = sourceUrl.lastIndexOf('.');
    String fileFormat = sourceUrl.substring(lastIndex);
    if (fileFormat.startsWith(".")) {
      MailFormatEnum.formatText(fileFormat.replaceFirst("\\.", ""));
    }
    this.sourceUrl = sourceUrl;
  }
}
