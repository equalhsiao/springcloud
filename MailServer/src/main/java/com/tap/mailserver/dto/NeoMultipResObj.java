package com.tap.mailserver.dto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.Data;

@Data
public class NeoMultipResObj {
	private String RETURNVALUE;
	//private List<String> ERRORPRIORITYS;
	private List<String> errorMultiRequestKeys;
	private String RETURNMSG;
	private Map<String, Object> response;
	private boolean isSuccess;

	public NeoMultipResObj() {
		this.response = new HashMap<>();
		this.errorMultiRequestKeys = new ArrayList<>();
	}

}
