package com.tap.mailserver.dto;

import com.tap.mailserver.enums.ErrorCode;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Base implements Serializable {

	private static final long serialVersionUID = 1518067360807690340L;
	protected int code;
	protected boolean isSuccess;
	protected Map<String, Object> data;
	protected String errMsg;
	protected String errId;

	public Base() {
		this.data = new HashMap<>();
		// TODO Auto-generated constructor stub
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
		this.data = new HashMap<>();
		this.isSuccess = code == ErrorCode.success;
	}

	public void putData(String key, Object value) {
		this.data.put(key, value);
	}

	public Map<String, Object> getData() {
		return data;
	}

	public void setData(Map<String, Object> data) {
		this.data = data;
	}

	public String getErrMsg() {
		return errMsg;
	}

	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}

	public String getErrId() {
		return errId;
	}

	public void setErrId(String errId) {
		this.errId = errId;
	}

	public boolean isSuccess() {
		return isSuccess;
	}

	public void setSuccess(boolean isSuccess) {
		this.isSuccess = isSuccess;
	}

}
