package com.tap.mailserver.dto;

import com.tap.mailserver.enums.ErrorCode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NeoResObj extends Base {
	public NeoResObj() {
		super();
	}

	public NeoResObj(int code) {
		super();
		super.code = code;
		super.isSuccess = code == ErrorCode.success;
	}

	public NeoResObj(int code, String errMsg) {
		super();
		super.code = code;
		super.isSuccess = code == ErrorCode.success;
		super.errMsg = errMsg;
	}

	public NeoResObj(boolean isSuccess, String errMsg) {
		super();
		super.isSuccess = isSuccess;
		super.errMsg = errMsg;
	}

	@SuppressWarnings("unchecked")
	public Map<String, Object> getMap() {
		if (this.isSuccess) {
			return (Map<String, Object>) data.get("items");
		} else {
			return new HashMap<>();
		}
	}

	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getList() {
		if (this.isSuccess) {
			return (List<Map<String, Object>>) data.get("items");
		} else {
			return new ArrayList<>();
		}
	}

	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getList(String key) {
		if (this.isSuccess) {
			return (List<Map<String, Object>>) data.get(key);
		} else {
			return new ArrayList<>();
		}
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> getList(Class<T> clazz) {
		if (this.isSuccess) {
			return (List<T>) data.get("items");
		} else {
			return new ArrayList<>();
		}
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> getList(String key, Class<T> clazz) {
		if (this.isSuccess) {
			return (List<T>) data.get(key);
		} else {
			return new ArrayList<>();
		}
	}
}
