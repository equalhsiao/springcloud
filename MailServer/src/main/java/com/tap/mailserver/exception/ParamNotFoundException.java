package com.tap.mailserver.exception;

public class ParamNotFoundException extends RuntimeException {
	public ParamNotFoundException() {
		super("Parameter not found!! 參數未正確填寫, 注意是否正確填入參數");
	}

	public ParamNotFoundException(String errMsg) {
		super(errMsg);
	}
}
