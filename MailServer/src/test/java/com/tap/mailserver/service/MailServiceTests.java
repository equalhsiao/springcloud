package com.tap.mailserver.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.mail.javamail.JavaMailSender;
import org.thymeleaf.ITemplateEngine;

import com.tap.mailserver.dto.Mail;
import com.tap.mailserver.dto.MailDtoTest;
import com.tap.mailserver.service.impl.MailServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class MailServiceTests {

	@InjectMocks
	private MailServiceImpl mailServiceImpl;

	@Mock
	private JavaMailSender javaMailSender;

	@Mock
	private ITemplateEngine emailTemplateEngine;

	@Test
	public void sendMailSuccess() throws Exception {
		Mail mail = new MailDtoTest().getInitMail();
		mailServiceImpl.sendMail(mail,null);
		Assert.assertTrue(true);
	}
}
