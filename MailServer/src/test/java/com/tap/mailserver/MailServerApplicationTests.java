package com.tap.mailserver;

import java.io.IOException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.Getter;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = MailServerApplication.class)
@WebAppConfiguration
@Getter
@AutoConfigureMockMvc
class MailServerApplicationTests {
	@Autowired
	protected MockMvc mockMvc;

	@Test
	void contextLoads() {
	}

	protected JSONArray jsonStringToJsonArray(String jsonString) throws JSONException {
		return new JSONArray(jsonString);
	}

	protected JSONObject jsonStringToJsonObject(String jsonString) throws JSONException {
		return new JSONObject(jsonString);
	}

	protected String mapToJson(Object obj) throws JsonProcessingException {
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.writeValueAsString(obj);
	}

	protected <T> T mapFromJson(String json, Class<T> clazz)
			throws JsonParseException, JsonMappingException, IOException {

		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.readValue(json, clazz);
	}
}
