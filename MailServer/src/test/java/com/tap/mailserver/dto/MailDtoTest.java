package com.tap.mailserver.dto;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class MailDtoTest {

	public Mail getInitMail() {
		Mail mail = new Mail();
		mail.setTo("equal.hsiao@tapsystem.co");
		mail.setSubject("單元測試");
		mail.setTemplateName("test");
//		Set<AttachmentFile> attachmentFileSet = new HashSet<AttachmentFile>();
//		AttachmentFile attachmentFile = new AttachmentFile();
//		attachmentFile.setFileFormat(MailFormatEnum.formatText("TXT"));
//		attachmentFile.setFileName("test");
//		attachmentFile.setFilePath("C:\\Users\\xyz90\\Downloads\\政府行政機關辦公日曆表.json");
//		attachmentFileSet.add(attachmentFile);
//		mail.setAttachmentFile(attachmentFileSet);
		Set<AttachmentUrl> attachmentUrlSet = new HashSet<AttachmentUrl>();
		AttachmentUrl attachmentUrl = new AttachmentUrl();
		attachmentUrl.setAttachmentFileFormat(MailFormatEnum.formatText("PDF"));
		attachmentUrl.setAttachmentFileName("test");
		attachmentUrl.setSourceUrl("https://storage.googleapis.com/ks-tw/Invoice/1574060662352/1574061789375.pdf");
		attachmentUrlSet.add(attachmentUrl);
		mail.setAttachmentUrl(attachmentUrlSet);
		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("project", "TAP");
		variables.put("author", "equalhsiao");
		variables.put("code", "666666666");
		mail.setVariables(variables);
		return mail;
	}
}
