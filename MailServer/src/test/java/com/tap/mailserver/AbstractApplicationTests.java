package com.tap.mailserver;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;

import lombok.Getter;

//@Rollback(true)
//@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = MailServerApplication.class)
@WebAppConfiguration
@Getter
@AutoConfigureMockMvc
public class AbstractApplicationTests extends BaseTests{


	@Autowired
	protected MockMvc mockMvc;

}
