package com.tap.mailserver.controller;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.tap.mailserver.AbstractApplicationTests;
import com.tap.mailserver.dto.MailDtoTest;
import com.tap.mailserver.utils.JsonTools;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MailControllerWithApplicationTests extends AbstractApplicationTests {

	private final static String URI = "/sendMail";

	@Test
	public void mailSendSuccess() throws Exception {
		String inputJson = JsonTools.mapToJson(new MailDtoTest().getInitMail());
		log.info("inputJson:" + inputJson);
		MvcResult mvcResult = mockMvc.perform(
				MockMvcRequestBuilders.post(URI).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
				.andReturn();
		int status = mvcResult.getResponse().getStatus();
		Assert.assertEquals(200, status);
		String content = mvcResult.getResponse().getContentAsString();
		JSONObject result = new JSONObject(content);
		Assert.assertTrue((int) result.get("code") == 0);
		Assert.assertTrue((Boolean) result.get("success"));

	}
}
