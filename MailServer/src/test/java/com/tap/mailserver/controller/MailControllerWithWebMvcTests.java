package com.tap.mailserver.controller;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.tap.mailserver.dto.MailDtoTest;
import com.tap.mailserver.service.MailService;
import com.tap.mailserver.utils.JsonTools;

import lombok.extern.slf4j.Slf4j;

/**
 * @author equalhsiao
 *
 */
@Slf4j
@RunWith(SpringRunner.class)
@WebMvcTest(MailController.class)
public class MailControllerWithWebMvcTests {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private MailService mailService;

	private final static String URI = "/sendMail";

	@Test
	public void sendMailTest() throws Exception {
		String inputJson = JsonTools.mapToJson(new MailDtoTest().getInitMail());
		log.info("inputJson:" + inputJson);
		MvcResult mvcResult = mockMvc.perform(
				MockMvcRequestBuilders.post(URI).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
				.andReturn();
		int status = mvcResult.getResponse().getStatus();
		Assert.assertEquals(200, status);
		String content = mvcResult.getResponse().getContentAsString();
		JSONObject result = new JSONObject(content);
		Assert.assertTrue((int) result.get("code") == 0);
		Assert.assertTrue((Boolean) result.get("success"));
	}
}
