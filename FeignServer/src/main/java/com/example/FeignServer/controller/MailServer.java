package com.example.FeignServer.controller;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MailServer {
	@Resource
	IMailServer iMailServer;

	@PostMapping(path = "/sendMail", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Map<String, Object> sendMail(@RequestBody Map<String, Object> mail) {
		return iMailServer.sendMail(mail);
	}
}
